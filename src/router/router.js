import {createBrowserRouter} from "react-router-dom";
import {toast} from "react-hot-toast";

const router = createBrowserRouter([
    {
        path: "/",
        element: (<button className="btn btn-neutral" onClick={() => toast.success("Hello World!")}>Neutral</button>
        ),
    },
]);

export default router