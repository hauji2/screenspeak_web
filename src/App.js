import './App.css';
import {Toaster} from "react-hot-toast";
import {RouterProvider} from "react-router-dom";
import router from "./router/router";

function App() {
    return (
        <div className="App">
            <div><Toaster/></div>
            <RouterProvider router={router}/>
        </div>
    );
}

export default App;
